import copy
from Objects.TrackedObject import *
import config as cfg
import os

#absolutna cesta ku root priecinku projektu
datasetPath = '/home/juraj/dataset/hliny6'


'''nastabenie darknet yolo'''
home = os.path.dirname(os.path.realpath(__file__))

#nastavi v subore cfg/coco.data absolutnu cestu k suboru data/coco.names; bez toho yolo nepojde
def set_darknet_home_path():
    new_content = ""
    with open(home + "/Detection/cfg/coco.data", 'r') as data_file:
        lines = data_file.read().split("\n")
        for line_content in lines:
            line = line_content.split("=")
            if line[0] == "names":
                new_content += line[0] + "=" + home + "/Detection/data/coco.names"
            else:
                new_content += line_content + "\n"
    data_file.close()

    with open(home + "/Detection/cfg/coco.data", "w") as outf:
        outf.write(new_content)
    outf.close()


set_darknet_home_path()
#nedovol pouzit yolo3 ak zaroven pouzivas aj tracker re3
if cfg.tracker_type == 3:
    cfg.darknet_version = 3
else:
    cfg.darknet_version = 2
#importuj prave tu verziu yolo ktora je nastavena
if cfg.darknet_version == 2:
    # yolo2 full coco
    import Detection.darknet as dn
    net = dn.load_net(home + "/Detection/cfg/yolov2.cfg", home + "/Detection/weights_file/yolov2.weights", 0)
    meta = dn.load_meta(home + "/Detection/cfg/coco.data")
elif cfg.darknet_version == 3:
    # yolo3 full coco   -nevmesti sa do pamati naraz aj tracker aj yolo3: OOM when allocating tensor with ...
    import Detection.darknet3 as dn
    net = dn.load_net(home + "/Detection/cfg/yolov3.cfg", home + "/Detection/weights_file/yolov3.weights", 0)
    meta = dn.load_meta(home + "/Detection/cfg/coco.data")


'''funkcie'''

#nacita obrazok a vrati 2 kopie
#parameter:
# path = cesta k suboru
# return 2 kopie obrazka
def load_image(path):
    image = cv2.imread(path)
    return image, copy.copy(image)


# vykresli vsetko do obrazku
#parametre:
# obrazok typu opencv BGR
# oblasti roi, exits, ignore
# tracked_objs = trasovane objekty
# draw_tracks = ak False, nevykreslia sa tracky
def draw_everything(image, my_rois=None, my_exits=None, my_ignores=None, tracked_objs=None, draw_tracks=True):
    if my_rois:
        for roi in my_rois:
            my_roi = roi.getBbox()
            cv2.rectangle(image, (my_roi[0], my_roi[1]), (my_roi[2], my_roi[3]), [0, 255, 0], 4)
    if my_exits:
        for exit in my_exits:
            ex = exit.getBbox()
            cv2.line(image, (ex[0], ex[1]), (ex[2], ex[3]), (255, 0, 0), 2)
    if my_ignores:
        for area in my_ignores:
            ar = area.getBbox()
            cv2.rectangle(image, (ar[0], ar[1]), (ar[2], ar[3]), [32, 32, 32], 3)

    if tracked_objs:
        for tracked in tracked_objs:
            if draw_tracks:
                tracked.draw_track(image)
            else:
                tracked.draw_box_only(image)


# najdi vsetky objekty v obrazku, prejdi vsetky detekcie a konvertuj ich na TrackedObject
# ak je v configu nastavene aby sa kreslili vsetky detekcie, vykreslia sa
# parametre:
# image_path = absolutna cesta k obrazku na disku
# image = obrazok typu opencv BGR
# return:
# vrati zoznam detekcii = tuple{ (class, conf, centered[x0,y0,x1,y1]) }
#####     plus
def detect_image(image_path, image=None):
    '''# type: (object, object, object) -> List[TrackedObject]'''
    detections = dn.detect(net, meta, image_path, cfg.det_conf_tresh, cfg.det_conf_tresh)
    # class, confidence, (centered x and y, width, height)
    detections_list = [list(i) for i in detections if i[0] in cfg.det_classes]
    return_list = []
    for det in detections_list:
        cls = det[0]
        conf = det[1]
        box = det[2]
        det = TrackedObject(int(box[0]), int(box[1]), int(box[2]), int(box[3]), cls, conf)
        #hned na zaciatku vyrad male objekty #a objekty na okrajoch
        filter = det.get_box_width < cfg.min_object_w or det.get_box_height < cfg.min_object_h #or \
            #det.x0 < cfg.min_dist_border or det.y0 < cfg.min_dist_border or \
            #det.x1 > (cfg.image_w - cfg.min_dist_border) or det.y1 > (cfg.image_h - cfg.min_dist_border)
        if not filter:
            return_list.append(det)
        if cfg.draw_all_detections and image is not None:
            cv2.rectangle(image, (int(box[0]), int(box[1])),
                          (int(box[2]), int(box[3])), [255, 0, 0], 2)
    return return_list


# prejdi vsetky detekcie a vrat tie co maju vyrazny prienik s roi a nemaju prienik s ignore maskami
# a nemaju vyrazny prienik s uz trasovanymi (ak sa detekovany objekt uz trasuje, ale znova bol zachyteny v roi)
# parametre:
# all_detections = zoznam vsetkych dekcii
# oblasti roi, exits a ignores
# tracked_objects = zoznam trasovanych objektov
# return:
# zoznam objektov typu TrackedObjects vratane tych co uz su trasovane
def filter_detections_roi_only(all_detections, my_rois, my_ignores, tracked_objects):
    return_list = []
    for det in all_detections:
        accept = False
        # prejdi vsetky roi ci tam napadne
        for roi in my_rois:
            if det.int_over_object_area(roi) >= cfg.min_roi_IoA:
                det.entry_roi = roi
                accept = True
                break
        if not accept:
            continue
        # zaroven prejdi vsetky ignore oblasti ci tam nepadne
        for ignore in my_ignores:
            if det.int_over_object_area(ignore) >= cfg.max_ignore_IoA:
                accept = False
                continue
        # na zaver skontroluj ci sa detekcia moc neprekryva s inymi uz traovanymi
        if tracked_objects:
            for tracked in tracked_objects:
                if det.int_over_object_area(tracked) >= cfg.max_tracked_detected_IoA:
                    accept = False
                    continue
        if accept:
            det.assign_id()
            return_list.append(det)
    return return_list


# prejdi vsetky objekty v liste tracked_objects a ak niektory splna podmienky na ukoncenie trasovani, vyrad ho
# parametre:
# tracked_objects = zoznam trasovanych objektov
# prislusne oblasti podla nazvu, ulozene v listoch
def check_tracked_for_exit(tracked_objects, my_exits=None, my_ignores=None):
    if not tracked_objects:
        return
    for tracked in tracked_objects:
        stop_tracking = False
        #skontroluj exity
        if my_exits:
            for exit in my_exits:
                if exit.has_intersection_line(tracked):
                    stop_tracking = True
                    break
        if my_ignores:
            for ignore in my_ignores:
                if ignore.has_intersection(tracked):
                    stop_tracking = True
                    break

        #ak nastala ina podmienka zrusenia kvoli odchodu mimo obrazu
        if tracked.is_out_of_view():
            stop_tracking = True
        #ak objekt prilis vela krat za sebou nebol najdeny -nemal ziadneho kandidata
        if tracked.times_not_found >= cfg.max_not_found_delete:
            stop_tracking = True
        #vyrad objekt ak nastala podmeinka zrusenia
        if stop_tracking:
            tracked_objects.remove(tracked)
    return tracked_objects

#konvertuj trasovane objekty z aktualnej snimky do formatu xml
# parametre:
# tracked_objects = zoznam trasovanych objektov
# image_path = cesta k aktualnej snimke
# retrun:
# string obsahujuci xml reprezentaciu objektov v snimku
def convert_tracked_to_xml(tracked_objects, image_path):
    split_path = image_path.split("/")
    out_file = split_path[-2] + "\\" + split_path[-1]
    res = '<image>\n\
      <src>' + out_file + '</src>\n\
      <boundingboxes>\n'

    if tracked_objects:
        for tracked in tracked_objects:
            res += '<boundingbox>\n\
            <x_left_top>'+ str(tracked.x0) +'</x_left_top>\n\
            <y_left_top>'+ str(tracked.y0) +'</y_left_top>\n\
            <width>'+ str(tracked.get_box_width()) +'</width>\n\
            <height>'+ str(tracked.get_box_width()) +'</height>\n\
            <class_name>\n\
            <project_id>'+ tracked.clas +'</project_id>\n\
            <confidence>'+ str(round(tracked.confidence,2)) +'</confidence>\n\
            <track_id>'+ str(tracked.id) +'</track_id>\n'
            res += '</class_name>\n\
            </boundingbox>\n'

    res += '</boundingboxes>\n\
    </image>\n'
    return res


#ulozi vstupny retazec do xml suboru, cesta a nazov suboru sa generuju automaticky
# parametre:
# xml_string = string s udajmi vo forme xml
def saveXml(xml_string):
    header = '<data> \
    \n<metadata> \
    \n<data_id>dataset_cars</data_id> \
    \n<parent/> \
    \n<version_major>2</version_major> \
    \n<xml_sid/> \
    \n<description>Anotacny nastroj v1.02</description> \
    \n</metadata> \
    \n<images>\n'

    footer = '\n</images> \
    \n</data>'

    xml_string = header + xml_string + footer

    with open(cfg.datasetPath + '.xml', "w") as outf:
        outf.write(xml_string)
    outf.close()


#zisti format obrazku
#parametre:
# fname = cesta k suboru
# return:
# string obsahujuci format suboru bez bodky
def get_image_format(fname):
    return fname.split(".")[-1]


#zisti rozmery obrazky, vo forme: sirka, vyska
# -zdroj: internet
#parametre:
# fname = cesta k suboru
# return:
# vyska a sirka suboru vo forme pola []
def get_image_size(fname):
    import struct, imghdr, re
    '''Determine the image type of fhandle and return its size.
    from draco'''
    while True:
        with open(fname, 'rb') as fhandle:
            head = fhandle.read(32)
            if len(head) != 32:
                break
            if imghdr.what(fname) == 'png':
                check = struct.unpack('>i', head[4:8])[0]
                if check != 0x0d0a1a0a:
                    break
                width, height = struct.unpack('>ii', head[16:24])
            elif imghdr.what(fname) == 'gif':
                width, height = struct.unpack('<HH', head[6:10])
            elif imghdr.what(fname) == 'jpeg':
                try:
                    fhandle.seek(0) # Read 0xff next
                    size = 2
                    ftype = 0
                    while not 0xc0 <= ftype <= 0xcf:
                        fhandle.seek(size, 1)
                        byte = fhandle.read(1)
                        while ord(byte) == 0xff:
                            byte = fhandle.read(1)
                        ftype = ord(byte)
                        size = struct.unpack('>H', fhandle.read(2))[0] - 2
                    # We are at a SOFn block
                    fhandle.seek(1, 1)  # Skip `precision' byte.
                    height, width = struct.unpack('>HH', fhandle.read(4))
                except Exception: #IGNORE:W0703
                    break
            elif imghdr.what(fname) == 'pgm':
                header, width, height, maxval = re.search(
                    b"(^P5\s(?:\s*#.*[\r\n])*"
                    b"(\d+)\s(?:\s*#.*[\r\n])*"
                    b"(\d+)\s(?:\s*#.*[\r\n])*"
                    b"(\d+)\s(?:\s*#.*[\r\n]\s)*)", head).groups()
                width = int(width)
                height = int(height)
            elif imghdr.what(fname) == 'bmp':
                _, width, height, depth = re.search(
                    b"((\d+)\sx\s"
                    b"(\d+)\sx\s"
                    b"(\d+))", str).groups()
                width = int(width)
                height = int(height)
            else:
                break
            return width, height
    imShape = cv2.imread(fname).shape
    return imShape[1], imShape[0]

#rozparsuj argumenty prikazoveho riadku
def parse_and_set_args(args):

    for a, arg in enumerate(args):
        strarg = str(arg)
        if strarg.startswith('-'):
            if strarg == "-input":
                cfg.datasetPath = args[a+1]
                continue
            elif strarg == "-algo":
                id = int(args[a+1])
                cfg.tracker_type = id
                if id == 2 or id == 3:
                    cfg.darknet_version = id
                continue
            elif strarg == "-simg":
                cfg.save_output_image = True
                continue
            elif strarg == "-sxml":
                cfg.save_output_xml = True
                continue
            elif strarg == "-dbg":
                cfg.debug_candidates = True
                continue
            elif strarg == "-ddet":
                cfg.draw_all_detections = True
                continue
            else:
                print 'warning - unknown argument: ' + arg
    return 0

'''
            ***PREPINACE***
-input + cesta_ku_videu  = cesta ju videu
-algo 1/2/3/4 = typ trasovania

-simg = povolit ukladanie vystupnych obrazkov
-sxml = povolit ukladanie vystupov vo formate xml

-dbg = povolit debugovanie v pripade spajania detekcii
-ddet = povolit kreslenie vsetkych detekcii 

'''

