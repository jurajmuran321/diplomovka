import config as cfg
import copy

#v pripade pouzitia trackera Re3 je nutne znizit verziu YOLO na 2 kvoli tomu ze 3 sa nevmesti do GPU RAM
if cfg.tracker_type == 1:
    if cfg.darknet_version == 3:
        cfg.darknet_version = 2
    import My_trackers.my_tracker_re3 as mt
elif cfg.tracker_type == 2 or cfg.tracker_type == 3:
    import My_trackers.my_tracker_yolo as mt
elif cfg.tracker_type == 4:
    if cfg.darknet_version == 3:
        cfg.darknet_version = 2
    import My_trackers.my_tracker_merge as mt


import helper as h
from Objects.TrackedObject import *


#trieda pouzivajuca sa na trasovanie; obsahuje hlavnu metodu track()
#ktora podla svojho nastavenia pouzije na trasovanie zvoleny algoritmus
class MyTracker(object):

    #konstruktor
    #parametre:
    # typ algoritmu (popisane pri metode track())
    # oblasti roi, exit a ignore vo forme pola []
    def __init__(self, algo_id, my_rois, my_exits, my_ignores):
        self.set_areas(my_rois, my_exits, my_ignores)
        self.algo_id = algo_id

    # nastav oblasti trasovania
    # parametre:
    # oblasti roi, exit a ignore vo forme pola []
    def set_areas(self, my_rois, my_exits, my_ignores):
        self.my_rois = my_rois
        self.my_exits = my_exits
        self.my_ignores = my_ignores


    #vykonaj trasovanie objektov v snimke danej obrazkom, pouzi typ metody ktoru si zvolil v konstruktore
    # typ trasovania
    # 1 = trasovanie iba pomocou trackera Re3, inicializacia detekciou yolo2
    # 2 = trasovanie iba cez spajanie detekcii z detektora yolo 2
    # 3 = trasovanie iba cez spajanie detekcii z detektora yolo 3
    # 4 = trasovanie kombinacie trackera a spajania detekcii z yolo2
    # parametre:
    # tracked_objects = trasovane objekty typu TrackedObjects vo forme pola []
    # tracked_objects = obrazok v standardnom opencv BGR formate nacitany cey imread
    # image_path = cesta ku obrazku na disku
    # frame_num = poradovae cislo obrazku od zaciatku trasovania
    # return:
    # nove vytrasovane polohy objektov typu TrackedObjects vo forme pola []
    def track(self, tracked_objects, image, image_path, frame_num):
        if self.algo_id == 1:
            return self.tracking_re3(tracked_objects, image, image_path, frame_num)
        elif self.algo_id == 2 or self.algo_id == 3:
            return self.tracking_yolo_only(tracked_objects, image, image_path, frame_num)
        elif self.algo_id == 4:
            return self.tracking_yolo_plus_re3(tracked_objects, image, image_path, frame_num)


    #test trasovanie len s pouzitim trackera, inicializacia detekcou yolo2
    # parametre a return zhodne s metodou track()
    def tracking_re3(self, tracked_objects, image, image_path, frame_num):
        # detekcia kazdych T krokov na najdenie novych prichadzajucich objektov
        new_objects = []
        if (frame_num == 1 or frame_num % cfg.detection_period_roi == 0):
            all_detections = h.detect_image(image_path, image)
            new_objects = h.filter_detections_roi_only(all_detections, self.my_rois, self.my_ignores, tracked_objects)
        # trasovanie
        imageRGB = image[:, :, ::-1]  # Tracker potrebuje RGB, opencv cita BGR.
        tracked_objects = mt.track_image(imageRGB, tracked_objects, new_objects)
        # skontroluj podmienky pre zrusenie objektu a vyrad ak treba
        tracked_objects = h.check_tracked_for_exit(tracked_objects, self.my_exits, self.my_ignores)
        return tracked_objects


    #test trasovanie bez trackera, iba s pouzitim spajania detekcii, yolo2 alebo yolo3, to si zvoli pouzivatel
    # parametre a return zhodne s metodou track()
    def tracking_yolo_only(self, tracked_objects, image, image_path, frame_num):
        #tu robis detekciu kazdy snimok
        all_detections = h.detect_image(image_path, image)
        # vykonaj kazdych T na najdenie novych prichadzajucich objektov
        new_objects = []
        if (frame_num == 1 or frame_num % cfg.detection_period_roi == 0):
            new_objects = h.filter_detections_roi_only(all_detections, self.my_rois, self.my_ignores, tracked_objects)
        image_backup = copy.copy(image)
        # trasovanie
        tracked_objects = mt.track_image_yolo(tracked_objects, all_detections, image_backup)
        # pridaj nove objekty najdene pre trasovanie
        if tracked_objects is None:
            tracked_objects = new_objects
        else:
            tracked_objects += new_objects
        # skontroluj podmienky pre zrusenie objektu a vyrad ak treba
        tracked_objects = h.check_tracked_for_exit(tracked_objects, self.my_exits, self.my_ignores)
        return tracked_objects


    #test trasovania kombinaciou trackera a spajania detekcii z yolo2
    # parametre a return zhodne s metodou track()
    def tracking_yolo_plus_re3(self, tracked_objects, image, image_path, frame_num):
        new_objects = []
        imageRGB = image[:, :, ::-1]  # Tracker potrebuje RGB, opencv cita BGR.
        # detekcia kazdych T krokov na najdenie novych prichadzajucich objektov a zistenie kandidatov
        if (frame_num == 1 or frame_num % cfg.detection_period_roi == 0):
            all_detections = h.detect_image(image_path, image)
            new_objects = h.filter_detections_roi_only(all_detections, self.my_rois, self.my_ignores, tracked_objects)
            #len v tomto kroku mozu pribudnut nove objekty, a mozu byt pouzite vystupy z detekcie
            #trasuj spajanim detekcii
            tracked_objects = mt.track_image_merge(imageRGB, tracked_objects, True, new_objects, all_detections)
        else:
            #trasuj normalne cez Re3
            tracked_objects = mt.track_image_merge(imageRGB, tracked_objects, False)
        # skontroluj podmienky pre zrusenie objektu a vyrad ak treba
        tracked_objects = h.check_tracked_for_exit(tracked_objects, self.my_exits, self.my_ignores)
        return tracked_objects
