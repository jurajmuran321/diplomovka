from Re3_tracker.tracker import re3_tracker
tracker = re3_tracker.Re3Tracker()


# konvertuj trasovane ale aj nove objekty do formy aby mohli byt pouzite ako vstup pre tracker Re3
#parametre
# tracked_objects: aktualne trasovane objekty typu TrackedObject
# new_list: nove prave pridene objekty typu TrackedObject
#return:
# vrati zoznam idciek objektov na trasovanie, a dict novych objektov
# ak nejake su vo forme id -> init_bbox[x0,y0, x1,y1]
def convert_for_tracker(tracked_objects, new_list=None):
    id_list = []
    new_bboxes = dict()
    if tracked_objects:
        for obj in tracked_objects:
            id_list.append(obj.id)
    if new_list :
        for obj in new_list:
            box = obj.getBbox()
            new_bboxes[obj.id] = box
            id_list.append(obj.id)
    return id_list, new_bboxes


# updatni pozicie trasovanych objektov po odtrasovani cez tracker Re3
#parametre:
# tracked_objects: aktualne trasovane objekty typu TrackedObject
# new_objects: nove prave pridene objekty typu TrackedObject
# tracked_boxes: nove bboxy vsetkych trasovanych a novych objektov po odtrasovani cez Re3
# id_list: list id-ciek pre bboxy, aby sa dali sparovat nove bboxy a trasovane objekty
#return
# vrati updatenuty zoznam trasovanych objektov ako jeden list
def convert_from_tracker(tracked_objects, new_objects, tracked_boxes, id_list):
    #nahadz si vsetky objekty do slovnika id -> objekt s danym id
    help_dict = dict()
    if not tracked_objects:
        merged_list = new_objects
    elif not new_objects:
        merged_list = tracked_objects
    else:
        merged_list = tracked_objects + new_objects
    for obj in merged_list:
        help_dict[obj.id] = obj
    for i in range (0, len(id_list), 1):
        id = id_list[i]
        obj = help_dict[id]
        new_pos = tracked_boxes[i]
        obj.update_position(new_pos)
    return merged_list


# trasuj cez tracker Re3
#parametre
# opencv obrazok typu RGB
# tracked_objects: list aktualne trasovanych objektov typu TrackedObjects
# new_objects: zoznam novych objektov ktore prisli v tomto snimku
#vrati zoznam objektov s updatenutymi poziciami
def track_image(imageRGB, tracked_objects, new_objects):
    #ak su oba zoznamy prazdne, teda sa netrasuje nic a ani sa nic nove nenaslo
    if (not tracked_objects) and (not new_objects):
        return tracked_objects
    #konvertuj na zoznam idciek a slovnik id -> bbox pre nove bboxy pre tracker
    id_list, new_bboxes_dict = convert_for_tracker(tracked_objects, new_objects)
    tracked_boxes = []
    bbox = None
    #ak sa riesi len 1 objekt
    if id_list.__len__() == 1:
        id = id_list[0]
        #ak ziadne nove objekty
        if new_bboxes_dict.__len__() == 0:
            bbox = tracker.track(id, imageRGB)
        else:
            bbox = new_bboxes_dict[id]
            bbox = tracker.track(id, imageRGB, bbox)
        tracked_boxes.append(bbox)
    else: #riesi sa viac ako 1 objekt
        # ziaden novy objekt
        if new_bboxes_dict.__len__() != 0:
            tracked_boxes = tracker.multi_track(id_list, imageRGB, new_bboxes_dict)
        else:
            tracked_boxes = tracker.multi_track(id_list, imageRGB)
    tracked_objects = convert_from_tracker(tracked_objects, new_objects, tracked_boxes, id_list)
    return tracked_objects


