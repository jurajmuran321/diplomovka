from Objects.CandidateTrack import *
from Objects.QuadTree import *
import config as cfg
import copy


# prejdi vsetkych kandidatov a prirad kazdemu trasovanemu
# objektu kandidatov, teda detekcie co s objektom maju nenulovy prienik
#parametre:
# all_detections = zoznam vsetkych detekcii
# tracked_objects = zoznam aktuaklne trasovanych objektov
#return:
# vrat v slovniku vsetkych kandidatov co su v hre vo forme [cand.id] = cand
def create_candidates_and_assign_to_tracked(all_detections, tracked_objects):
    #all_detections = filter_detections(all_detections)
    candidates_list = []
    candidates_dict = dict()
    for det in all_detections:
        candidates_list.append(CandidateTrack(det))

    candidates_tree = QuadTree(candidates_list, 8, (0, 0, cfg.image_w, cfg.image_h))
    # pre kazdy trasovany objekt najdi kandidatov na novu polohu
    for tracked in tracked_objects:
        tracked.candidates = candidates_tree.hit(tracked)
        for cand in tracked.candidates:
            candidates_dict[cand.id] = cand
    return candidates_dict


# vypocitaj ake skore dosiahne dana deteckia pre dany trasovany objekt, zaznac v objekte jednotlive hodnoty
#parametre
# tracked: objekt typu TrackedObject, vzhladom na ktory sa pocita skore detekcie
# candidate: objekt typu Candidate, potencialna nova poloha objektu
def compute_candidates_score(tracked, candidate):
    iou = tracked.int_over_union(candidate)
    candidate.c_IoU = 1 - iou  # chcem penalizovat male IoU, preto 1 - IoU
    shift = tracked.center_distance_two_boxes(candidate)
    candidate.c_shift = shift
    wdth = abs(tracked.get_box_width() - candidate.get_box_width())
    candidate.c_width = wdth
    hght = abs(tracked.get_box_height() - candidate.get_box_height())
    candidate.c_height = hght
    ratio = abs(tracked.get_box_ratio_height_to_width() - candidate.get_box_ratio_height_to_width())
    candidate.c_ratio = ratio
    # celkove skore
    overall_score = iou * cfg.w_IoU + shift * cfg.w_shift + wdth * cfg.w_width + hght * cfg.w_height + ratio * cfg.w_ratio
    #zaznac skore kandidata vzhladom na trasovany, do slovnika podla id trasovaneho
    candidate.overall_score_dict[tracked.id] = overall_score
    # pre kazdy z kandidatov zaznac ze je kandidatom pre dany objekt, do slovnika podla tracked.id
    candidate.candidate_for_tracked_dict[tracked.id] = tracked


# trasuj cez YOLO sprajanim detekcii
#parametre:
# opencv obrazok typu RGB
# tracked_objects: list aktualne trasovanych objektov typu TrackedObjects
# all_detections: zoznam vsetkych detekcii
# image_back: obrazok typu Opencv BGR pre potreby debugovania
# use_kalman: ak True, pouzije sa pri strate objektu na jeho trasovanie kalmanov filter
#vrati zoznam objektov s updatenutymi poziciami
def track_image_yolo(tracked_objects, all_detections, image_back=None, use_kalman= True):
    # ak aktualne nic netrasujes, nic nerob
    if (not tracked_objects):
        return tracked_objects
    candidates_dict = create_candidates_and_assign_to_tracked(all_detections, tracked_objects)
    for tracked in tracked_objects:
        # prejdi cez vsetky detekcie a urci a vypocitaj skore jednotlivych kandidatov
        for candidate in tracked.candidates:
            compute_candidates_score(tracked, candidate)
    # vyhodnot kandidatov pre kazdy objekt
    for tracked in tracked_objects:
        if cfg.debug_candidates:
            image = copy.copy(image_back)
            cv2.rectangle(image, (tracked.x0, tracked.y0), (tracked.x1, tracked.y1), [0, 128, 128], 4)
            x, y = tracked.get_centered_x(), tracked.get_centered_y()
            cv2.putText(image, str(tracked.id), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2, cv2.LINE_8)
        min_score = 100000  #lubovolne velke cislo
        tracked.candidate_winner = None
        for candidate in tracked.candidates:
            if cfg.debug_candidates:
                cv2.rectangle(image, (candidate.x0, candidate.y0), (candidate.x1, candidate.y1), [128, 0, 0], 4)
                label = '=' + str(round(candidate.overall_score_dict[tracked.id], 2))
                cv2.putText(image, label,
                            (candidate.x0, candidate.y1), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 0, 255), 2, cv2.LINE_8)
            if candidate.overall_score_dict[tracked.id] < min_score:
                min_score = candidate.overall_score_dict[tracked.id]
                tracked.candidate_winner = candidate
        #zaznac si pre vitazneho kandidata ze je vitazom pre tento tracked
        if tracked.candidate_winner is not None:
            tracked.candidate_winner.assigned_tracked_dict[tracked.id] = tracked

        if cfg.debug_candidates:
            cv2.namedWindow('debug', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('debug', image)
            while True:
                key = cv2.waitKey(10)
                if key == ord('c'):
                    break
    #kazdemu trasovanemu bol priradeny prave jeden alebo ziaden vitaz
    #skontroluj ci boli vitazi priradeni spravne, a prirad nove bboxy aj tym co vitaza nemali
    post_process_winners_v2(tracked_objects, candidates_dict, use_kalman)
    CandidateTrack.next_candidate_id = 0  # len vynuluj idcka nech idu zasa od 0
    return tracked_objects


#skontroluj a oprav vzsledky yolo spajania
# riesi sa priradenie kandidata viacerym roznym trackom, ci nepriradenie ziadneho kandidata
#parametre:
# tracked_objects: list aktualne trasovanych objektov typu TrackedObjects
# candidates_dict: slovnik vsetkych kandidatov, vo forme [cand.id] -> cand
# use_kalman: ak True, pouzije sa pri strate objektu na jeho trasovanie kalmanov filter
#return
# vrati zoznam objektov s updatenutymi poziciami
def post_process_winners_v2(tracked_objects, candidates_dict, use_kalman = True):
    #tato cast odstrani viacnasobne priradenie kandidata ku tracked
    #prejdi cez kandidatov, a ries tych co su priradeni viac ako 1 tracku a hod ich do slovnika
    for cid, cand in candidates_dict.iteritems():
        #candidates co su priradeni viacerym tracked
        if len(cand.assigned_tracked_dict) > 1:
            only_winner = None
            min_score = 100000 #lubovolne velke cislo
            #najdi pre ktoreho z tracked dosiahol kandidat najnizsie skore
            for tid, tracked in cand.assigned_tracked_dict.iteritems():
                #zrus priradeneho kandidata
                tracked.candidate_winner = None
                tracked.times_not_found += 1
                #porovnaj skore tracked k tomuto kandidatovi
                if cand.overall_score_dict[tid] < min_score:
                    min_score = cand.overall_score_dict[tid]
                    only_winner = tracked
            only_winner.candidate_winner = cand
            only_winner.times_not_found = 0
            only_winner.last_score = cand.overall_score_dict[only_winner.id]

    #prejdi vsetko, vyhodnot vitazov, pripadne ofhadni nove pozicie, a sprav update polohy
    for tracked in tracked_objects:
        # ak sa nenasiel vobec ziadny kandidat, skus odhadnut novu poziciu na zaklade predosleho pohybu
        # alebo ak sa vitaz nasiel, ale mal vysoke skore, asi sa jedna o occlusion objekt, radsej odhadni novu polohu
        if tracked.candidate_winner is None or \
            (len(tracked.locations) > 3 and
             (tracked.candidate_winner.overall_score_dict[tracked.id] > (tracked.last_score + cfg.max_delta_score))):
            new_box = tracked.estimate_next_location()
            tracked.times_not_found += 1
            if not use_kalman:
                #return tracked_objects.remove(tracked)
                tracked_objects.remove(tracked)
        else:
            #nasiel sa vhodny vitaz, tu iba aktualizuj
            winner = tracked.candidate_winner
            tracked.times_not_found = 0
            if use_kalman:
                measured = np.array([[np.float32(winner.get_centered_x())], [np.float32(winner.get_centered_y())]])
                pred = tracked.kf.predict()  # len preto aby to fungovalo
                tracked.kf.correct(measured)
            tracked.last_score = winner.overall_score_dict[tracked.id]
            new_box = [winner.x0, winner.y0, winner.x1, winner.y1]
        tracked.update_position(new_box)
