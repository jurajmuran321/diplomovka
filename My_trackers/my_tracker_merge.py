import my_tracker_re3 as mt_re3
import my_tracker_yolo as mt_yolo
from Objects.TrackedObject import *

# trasuj spajanim detekcii, kde na spajanie pouzijes vystupy z trackera, a len
# kazdy n-ty krok trasujes s vystupmi z detektora kvoli validacii
# tracked_objects: list aktualne trasovanych objektov typu TrackedObjects
# is_detection: True, ak v tomto kroku prehla detekcia, teda su k dispozicii vystupy z nej
# new_objects: zoznam novych objektov ktore prisli v tomto snimku
# all_detections: zoznam detekcii z yolo
#vrati zoznam objektov s updatenutymi poziciami
def track_image_merge(imageRGB, tracked_objects, is_detection, new_objects=None, all_detections=None):
    #ak sa nic netrasuje
    if not tracked_objects:
        #ani sa nic nove nenaslo, nerob nic
        if new_objects is None:
            return None

    if not is_detection:
        # ak som v kroku kde sa trasuje len cez Re3 tracker, ziskaj kandidatov takto cez tracker
        candidate_boxes = get_tracker_estimates(imageRGB, tracked_objects)
        candidate_boxes = convert_estimates_tracker_to_candidates(candidate_boxes)
    else:
        # ak som v kroku detekcie, kandidati su vystupy z detekcie
        candidate_boxes = all_detections

    #trasuj cez "spajanie detekcii"
    tracked_objects = mt_yolo.track_image_yolo(tracked_objects, candidate_boxes, imageRGB, use_kalman=False)
    #pridaj nove objekty ak su
    if new_objects:
        add_new_objects(imageRGB, new_objects)
        if not tracked_objects:
            tracked_objects = new_objects
        else:
            tracked_objects += new_objects

    #aktualizuj tracker podla toho ako si priradil nove pozicie
    if is_detection:
        correct_tracker(imageRGB, tracked_objects)
    return tracked_objects

# prida do trasovania nove objekty z noveho snimku
#parametre:
# imageRGB = obrazok typu opencv RGB
# new_objects: zoznam novych objektov ktore prisli v tomto snimku
def add_new_objects(imageRGB, new_objects):
    id_list = []
    tracked_dict = dict()
    for obj in new_objects:
        id_list.append(obj.id)
        tracked_dict[obj.id] = obj.getBbox()
    # update poziciip
    boxes_list = []
    if len(id_list) == 1:
        id = id_list[0]
        boxes_list.append(mt_re3.tracker.track(id, imageRGB, tracked_dict[id]))
    else:  # riesi sa viac ako 1 objekt
        boxes_list = mt_re3.tracker.multi_track(id_list, imageRGB, tracked_dict)


#odhadni nove polohy objektov cez tracker a vrat len pole bboxov tychto poloh
#parametre:
# imageRGB = obrazok typu opencv RGB
# tracked_objects: list aktualne trasovanych objektov typu TrackedObjects
# new_objects: zoznam novych objektov ktore prisli v tomto snimku
#return
# vystupne boxy z trasovania v zozname [x0, y0, x1, y1]
def get_tracker_estimates(imageRGB, tracked_objects, new_objects=None):
    # konvertuj na zoznam idciek a slovnik id -> bbox pre nove bboxy pre tracker
    id_list, new_bboxes_dict = mt_re3.convert_for_tracker(tracked_objects, new_objects)
    tracked_boxes = []
    # ak sa riesi len 1 objekt
    if len(id_list) == 1:
        id = id_list[0]
        # ak ziadne nove objekty
        if len(new_bboxes_dict) == 0:
            bbox = mt_re3.tracker.track(id, imageRGB)
        else:
            bbox = new_bboxes_dict[id]
            bbox = mt_re3.tracker.track(id, imageRGB, bbox)
        tracked_boxes.append(bbox)
    else:  # riesi sa viac ako 1 objekt
        # ziaden novy objekt
        if len(new_bboxes_dict) != 0:
            tracked_boxes = mt_re3.tracker.multi_track(id_list, imageRGB, new_bboxes_dict)
        else:
            tracked_boxes = mt_re3.tracker.multi_track(id_list, imageRGB)

    return tracked_boxes

#konvertuj vystupne bboxy z trackera na kandidatov
#parametre:
# tracked_boxes = vystupne boxy z trasovania v zozname [x0, y0, x1, y1]
def convert_estimates_tracker_to_candidates(tracked_boxes):
    #nahadz si vsetky objekty do slovnika id -> objekt s danym id
    return_list = []
    for box in tracked_boxes:
        det = TrackedObject(int(box[0]), int(box[1]), int(box[2]), int(box[3]), "Unknown", 1)
        return_list.append(det)
    return return_list



#pre vsetky bboxy zadaj trackeru ich novu poziciu, ak sa zmenila
#parametre:
# imageRGB = obrazok typu opencv RGB
# tracked_objects = zoznam trasovanych objektov
def correct_tracker(imageRGB, tracked_objects):
    if not tracked_objects:
        return
    #priprav zoznam idciek a slovnik
    id_list = []
    tracked_dict = dict()
    for obj in tracked_objects:
        id_list.append(obj.id)
        tracked_dict[obj.id] = obj.getBbox()
    #update poziciip
    if len(id_list) == 1:
        id = id_list[0]
        mt_re3.tracker.track(id, imageRGB, tracked_dict[id])
    else:  # riesi sa viac ako 1 objekt
        mt_re3.tracker.multi_track(id_list, imageRGB, tracked_dict)
