from dircache import annotate

import cv2
import numpy as np
import glob
import xml.etree.ElementTree as ET
import time
import random
import os
import sys
import constants

from re3_utils.util.im_util import get_image_size
from my_constants import classes

dataset_path = '/media/juraj/ADATA UFD/dataset/data/' # pridat _train / _val

annotationPath = dataset_path + 'annotations/'
orig_ann = dataset_path + 'orig_labels/'
imagePath = dataset_path + 'images/'


videos = sorted(glob.glob(imagePath + '/*/'))
orig_labels = sorted(glob.glob(orig_ann + '*.txt'))
ann_objects = list

for vv,video in enumerate(videos):
    #if not video.__contains__("kamenna"):
    #    continue
    images = sorted(glob.glob(video + '*.png'))
    labels = [image.replace('images', 'annotations').replace('png', 'xml') for image in images]
    im_size = get_image_size(images[0])
    folder_name = annotationPath + video.replace(imagePath, '')
    if not os.path.exists(folder_name):
        os.makedirs(folder_name)
    #orig_labels_file = open(orig_labels[lab_ind], 'r')
    #orig_labels = orig_labels_file.read()


    f = open(orig_labels[vv], 'r')
    file_lines = f.readlines()
    f.close()

    #parse objects and store it into list
    ann_objects = [None] * images.__len__()
    image_item_ind = 0
    image_num = 0
    for i in range (0, file_lines.__len__(),2):
        name = file_lines[i].strip().lower()
        if name == 'end':
            image_num += 1
            continue
        elif name == 'none':
            continue
        elif name in classes:
            line = file_lines[i+1]
            data = line.split()
            #class, topleft x, y, bottomright x, y
            obj = [name, data[0], data[1], data[2], data[3]]
            if ann_objects[image_num] is None:
                ann_objects[image_num] = [None] * 50
                image_item_ind = 0
            ann_objects[image_num][image_item_ind] = obj
            image_item_ind += 1

    # ann_objects[image_num] = {[obj_1], [obj_2], ...., [obj_n], [None],  ... length = 50 } ; length = num_images
    image_ind = 0
    item_ind = 0
    for ll, label in enumerate(labels):
        #if ll == labels.__len__():
        #    break
        imageXml = open(label, 'w')
        folder_name = video.replace(imagePath, '')
        file_name = label.replace(annotationPath + folder_name, '').replace('.txt', '')

        result = \
            '<annotation>' + '\n' + \
            '   <folder>' +  folder_name + '</folder>' + '\n' + \
            '   <filename>' + file_name + '</filename>' + '\n' + \
            '   <source>' + '\n' + \
            '       <database>dataset</database>' + '\n' + \
            '   </source>' + '\n' + \
            '   <size>' + '\n' + \
            '       <width>' + str(im_size[0]) + '</width>' + '\n' + \
            '       <height>' + str(im_size[1]) + '</height>' + '\n' + \
            '       <depth>3</depth>' + '\n' + \
            '   </size>' + '\n' + \
            '   <segmented>0</segmented>' + '\n'
        # check for image with no objects
        frame_objects = ann_objects[ll]
        if  frame_objects is None:
            result += '</annotation>'
            imageXml.write(result)
            imageXml.close()
            continue
        # collect all object form current frame
        item_ind = 0
        while True:
            item = frame_objects[item_ind]
            if item is None:
                break
            trackId = 0
            name_class = item[0]
            occluded = '0'
            result += \
            '   <object>' + '\n' + \
            '       <name >'+ name_class +'</name>' + '\n' + \
            '       <pose>Unspecified</pose>' + '\n' + \
            '       <truncated>0</truncated>' + '\n' + \
            '       <difficult>0</difficult>' + '\n' + \
            '       <occluded>'+ occluded +'</occluded>' + '\n' + \
            '       <trackid>0</trackid>' + '\n' + \
            '       <bndbox>' + '\n' + \
            '           <xmin>'+ item[1] +'</xmin>' + '\n' + \
            '           <ymin>'+ item[2] +'</ymin>' + '\n' + \
            '           <xmax>'+ item[3] +'</xmax>' + '\n' + \
            '           <ymax>'+ item[4] +'</ymax>' + '\n' + \
            '       </bndbox>' + '\n' + \
            '   </object>'  + '\n'
            item_ind += 1
            #if item_ind == count:
            #    break
        result += '</annotation>'
        imageXml.write(result)
        imageXml.close()
    print folder_name + ' done!'



