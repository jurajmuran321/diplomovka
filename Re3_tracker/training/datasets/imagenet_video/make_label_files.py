import cv2
import numpy as np
import glob
import xml.etree.ElementTree as ET
import time
import random
import os
import sys

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(
    basedir,
    os.path.pardir,
    os.path.pardir,
    os.path.pardir)))

from re3_utils.util import drawing
from re3_utils.util.im_util import get_image_size

DEBUG = False

def main(label_type):
    #wildcard = '/*/*/' if label_type == 'train' else '/*/'
    wildcard = '/*/'
    dataset_path = '/media/juraj/Maxtor/dataset/'
    annotationPath = dataset_path + 'data_' + label_type + '/annotations/'
    imagePath = dataset_path + 'data_' + label_type + '/images/'

    if not DEBUG:
        if not os.path.exists(os.path.join(dataset_path, 'labels', label_type)):
            os.makedirs(os.path.join(dataset_path, 'labels', label_type))
        imageNameFile = open(dataset_path + 'labels/' + label_type + '/image_names.txt', 'w')

    videos = sorted(glob.glob(annotationPath + wildcard))

    bboxes = []
    imNum = 0
    totalImages = len(glob.glob(annotationPath + wildcard + '*.xml')) #.xml
    print 'totalImages', totalImages
    # 'Car', 'Van', 'Truck', 'Pedestrian', 'Person_sitting', 'Cyclist', 'Tram', 'Misc'

    classes_map = {
            'car': 1,
            'van': 2,
            'truck': 3,
            'bus': 4,
            'cyclist': 5,
            'pedestrian': 6,
            'person_sitting': 7,
            'tram': 8,
            'misc': 9,
            'person': 10,
            'n02419796': 11,
            'n02374451': 12,
            'n04530566': 13,
            'n02118333': 14,
            'n02958343': 15,
            'n02510455': 16,
            'n03790512': 17,
            'n02391049': 18,
            'n02121808': 19,
            'n01726692': 20,
            'n02062744': 21,
            'n02503517': 22,
            'n02691156': 23,
            'n02129165': 24,
            'n02129604': 25,
            'n02355227': 26,
            'n02484322': 27,
            'n02411705': 28,
            'n02924116': 29,
            'n02131653': 30,
            }

    for vv,video in enumerate(videos):
        labels = sorted(glob.glob(video + '*.xml'))
        images = [label.replace('annotations', 'images').replace('xml', 'png') for label in labels]
        trackColor = dict()
        for ii,imageName in enumerate(images):
            if imNum % 100 == 0:
                print 'imNum %d of %d = %.2f%%' % (imNum, totalImages, imNum * 100.0 / totalImages)
            if not DEBUG:
                # Leave off initial bit of path so we can just add parent dir to path later.
                imageNameFile.write(imageName + '\n')
            label = labels[ii]
            labelTree = ET.parse(label)
            imgSize = get_image_size(images[ii])
            area = imgSize[0] * imgSize[1]
            if DEBUG:
                print '\n%s' % images[ii]
                image = cv2.imread(images[ii])
                print 'video', vv, 'image', ii
            for obj in labelTree.findall('object'):
                cls = obj.find('name').text # name stands for class
                cls = str(cls).lower()
                try:
                    assert cls in classes_map
                except AssertionError:
                    print 'Unknown class ' + cls
                    return 1
                classInd = classes_map[cls]
                occl = 0
                if obj.find('occluded')is not None:
                    occl = int(obj.find('occluded').text)
                trackId = 0
                if obj.find('trackid'):
                    trackId = int(obj.find('trackid').text)
                bbox = obj.find('bndbox')
                bbox = [int(bbox.find('xmin').text),
                        int(bbox.find('ymin').text),
                        int(bbox.find('xmax').text),
                        int(bbox.find('ymax').text),
                        vv, trackId, imNum, classInd, occl]

                if DEBUG:
                    print 'name', obj.find('name').text, '\n'
                    print bbox
                    if trackId not in trackColor:
                        trackColor[trackId] = [random.random() * 255 for _ in xrange(3)]
                    drawing.drawRect(image, bbox[:4], 3, trackColor[trackId])
                bboxes.append(bbox)
            if DEBUG:
                cv2.imshow('image', image)
                cv2.waitKey(1)

            imNum += 1
        #break #pre 1 video
    bboxes = np.array(bboxes)
    # Reorder by video_id, then track_id, then video image number so all labels for a single track are next to each other.
    # This only matters if a single image could have multiple tracks.
    order = np.lexsort((bboxes[:,6], bboxes[:,5], bboxes[:,4]))
    bboxes = bboxes[order,:]
    if not DEBUG:
        np.save(dataset_path + 'labels/' + label_type + '/labels.npy', bboxes)

if __name__ == '__main__':
    main('train')
    main('val')

