from dircache import annotate

import cv2
import numpy as np
import glob
import xml.etree.ElementTree as ET
import time
import random
import os
import sys
import constants
import operator

from re3_utils.util.im_util import get_image_size
from my_constants import classes

dataset_path = '/media/juraj/Maxtor/dataset_cars/' # pridat _train / _val

annotationPath = dataset_path + 'annotations/'
orig_ann = dataset_path + 'yolo_detections/' # _all su vsetky data, pre test data priponu odstran
imagePath = dataset_path + 'images/'

min_confidence = 25 # v percentach

videos = sorted(glob.glob(imagePath + '/*/'))
orig_labels = sorted(glob.glob(orig_ann + '*.txt'))
#ann_objects = list

for vv,video in enumerate(videos):
    images = sorted(glob.glob(video + '*.png'))
    im_size = get_image_size(images[0])

    video_name = video.replace(imagePath, '')
    video_name = video_name.replace('/', '')
    detections_path = orig_ann + video_name + '.txt'
    f = open(detections_path, 'r')
    file_lines = f.readlines()
    f.close()

    ann_objects = dict()
    image_item_ind = 0
    image_num = 0
    image_name = ""
    image_info = ""
    no_objects = False
    for i in range (0, file_lines.__len__(),2):
        line1 = file_lines[i].strip().lower()
        line2 = file_lines[i+1].strip().lower()
        if line1 == 'filename':
            image_name = line2
            image_info = "  <image>\n" \
                         "   <src>" + video_name + "\\" + image_name + "</src>\n" \
                         "   <boundingboxes>\n"  #pre cestu pouzi windowsove spatne \
            no_objects = False
        elif line1 == 'none':
            image_info = "  <image>\n" \
                         "   <src>" + video_name + "\\" + image_name + "</src>\n" \
                         "   <boundingboxes/>\n" \
                         "  </image>\n" # pre cestu pouzi windowsove spatne \
            no_objects = True
            continue
        elif line1 == 'end':
            if not no_objects:
                image_info += "   </boundingboxes>\n" \
                              "  </image>\n"
            ann_objects[image_name] = image_info
        else:
            vals1 = line1.split()
            cl_ass = vals1[0]
            conf = vals1[1]
            # ak je to objekt z nechcenej triedy # alebo ma nizku confidence
            if (not cl_ass in classes): # or (conf < min_confidence):
                continue
            vals2 = line2.split()
            x1 = int(vals2[0])
            y1 = int(vals2[1])
            x2 = int(vals2[2])
            y2 = int(vals2[3])
            w = x2 - x1
            h = y2 - y1
            image_info += "    <boundingbox>\n" \
                          "     <x_left_top>" + str(x1) + "</x_left_top>\n" \
                          "     <y_left_top>" + str(y1) + "</y_left_top>\n" \
                          "     <width>" + str(w) + "</width>\n" \
                          "     <height>"+ str(h) + "</height>\n" \
                          "     <class_name>\n" \
                          "      <project_id>" + cl_ass + "</project_id>\n" \
                          "      <confidence>" + conf + " </confidence>\n" \
                          "      <track_id>0</track_id>\n" \
                          "     </class_name>\n" \
                          "    </boundingbox>\n"

    result = "<data>\n" \
             " <metadata>\n" \
             "  <data_id>dataset_cars</data_id>\n" \
             "  <parent/>\n" \
             "  <version_major>2</version_major>\n" \
             "  <xml_sid/>\n" \
             "  <description>Anotacny nastroj v1.02</description>\n" \
             " </metadata>\n" \
             " <images>\n"

    #utriedi data podla kluca a vrati zoznam tupple, kde kazdy jeho prvok je dvojica 0:[kluc], 1:[hodnota]
    ann_objects_sorted_by_image_name = sorted(ann_objects.items(), key=operator.itemgetter(0))

    for v, value in enumerate(ann_objects_sorted_by_image_name):
        result += value[1]

    result += " </images>\n" \
              "</data>"

    outPath = imagePath + video_name + '.xml'
    f = open(outPath, 'w')
    f.write(result)
    print video_name + ' done!'

'''
    for ll, label in enumerate(labels):
        #if ll == labels.__len__():
        #    break
        imageXml = open(label, 'w')
        folder_name = video.replace(imagePath, '')
        file_name = label.replace(annotationPath + folder_name, '').replace('.txt', '')

        result = \
            '<annotation>' + '\n' + \
            '   <folder>' +  folder_name + '</folder>' + '\n' + \
            '   <filename>' + file_name + '</filename>' + '\n' + \
            '   <source>' + '\n' + \
            '       <database>dataset</database>' + '\n' + \
            '   </source>' + '\n' + \
            '   <size>' + '\n' + \
            '       <width>' + str(im_size[0]) + '</width>' + '\n' + \
            '       <height>' + str(im_size[1]) + '</height>' + '\n' + \
            '       <depth>3</depth>' + '\n' + \
            '   </size>' + '\n' + \
            '   <segmented>0</segmented>' + '\n'
        # check for image with no objects
        frame_objects = ann_objects[ll]
        if  frame_objects is None:
            result += '</annotation>'
            imageXml.write(result)
            imageXml.close()
            continue
        # collect all object form current frame
        item_ind = 0
        while True:
            item = frame_objects[item_ind]
            if item is None:
                break
            trackId = 0
            name_class = item[0]
            occluded = '0'
            result += \
            '   <object>' + '\n' + \
            '       <name >'+ name_class +'</name>' + '\n' + \
            '       <pose>Unspecified</pose>' + '\n' + \
            '       <truncated>0</truncated>' + '\n' + \
            '       <difficult>0</difficult>' + '\n' + \
            '       <occluded>'+ occluded +'</occluded>' + '\n' + \
            '       <trackid>0</trackid>' + '\n' + \
            '       <bndbox>' + '\n' + \
            '           <xmin>'+ item[1] +'</xmin>' + '\n' + \
            '           <ymin>'+ item[2] +'</ymin>' + '\n' + \
            '           <xmax>'+ item[3] +'</xmax>' + '\n' + \
            '           <ymax>'+ item[4] +'</ymax>' + '\n' + \
            '       </bndbox>' + '\n' + \
            '   </object>'  + '\n'
            item_ind += 1
            #if item_ind == count:
            #    break
        result += '</annotation>'
        imageXml.write(result)
        imageXml.close()
'''




