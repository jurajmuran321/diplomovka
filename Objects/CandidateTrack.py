from TrackedObject import *

#reprezentacia kandidata na kovu poziciu objektu
class CandidateTrack(TrackedObject):

    next_candidate_id = 0

    #pretazeny konstruktor
    #vytvor novy objekt zo suradnic
    def __init__(self, x0, y0, x1, y1):
        RectObject.__init__(self, CandidateTrack.next_candidate_id, x0, y0, x1, y1)
        CandidateTrack.next_candidate_id += 1

        #slovnik [id_tracked_obj] = trasovany objekt
        self.candidate_for_tracked_dict = dict()
        #slovnik [id_tracked_obj] = skore kandidata pre dany trasovany objekt
        self.overall_score_dict = dict()
        #slovnik [id_tracked_obj] = trasovany objekt, teda slovnik objektov ktorym je priradeny ako vitaz
        self.assigned_tracked_dict = dict()


    #pretazeny konstruktor
    #vytvor novy objekt z detekcie (ta je typu tracked object)
    def __init__(self, tracked_object):
        RectObject.__init__(self, CandidateTrack.next_candidate_id, \
                            tracked_object.x0, tracked_object.y0, tracked_object.x1, tracked_object.y1)
        CandidateTrack.next_candidate_id += 1

        # slovnik [id_tracked_obj] = trasovany objekt
        self.candidate_for_tracked_dict = dict()
        #zoznam trasovanych objektov pre ktore je tato detekcia kandidatom
        # slovnik [id_tracked_obj] = skore kandidata pre dany trasovany objekt
        self.overall_score_dict = dict()
        # slovnik [id_tracked_obj] = trasovany objekt, teda slovnik objektov ktorym je priradeny ako vitaz
        self.assigned_tracked_dict = dict()

    #iba vynuluj id-cka nech idu znova od 0
    @staticmethod
    def reset_next_candidate_id():
        CandidateTrack.next_candidate_id = 0