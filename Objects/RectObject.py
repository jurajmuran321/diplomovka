import numpy as np
from math import sqrt

#trieda reprezentujuca 2D pravouhly objekt
# ma ulozene zakladnu udaje a vie s nimi robit niektore zakladne operacie
class RectObject(object):

    #konstruktor
    # id objektu, suradnice laveho horneho a praveho dolneho rohu
    def __init__(self, id, x0, y0, x1, y1):
        self.id = id
        self.x0 = x0
        self.y0 = y0
        self.x1 = x1
        self.y1 = y1


    #vrat suradnice objektu [left_top_xy, right_bottom_xy]
    def getBbox(self):
        return [self.x0, self.y0, self.x1, self.y1]


    # vrat velkost objektu v pixeloch
    def get_box_area(self):
        return (self.x1 - self.x0) * (self.y1 - self.y0)


    # vrati sirku objektu v pixeloch
    def get_box_width(self):
        return (self.x1 - self.x0)


    # vrati vysku objektu v pixeloch
    def get_box_height(self):
        return (self.y1 - self.y0)


    # vrati x suradnicu stredu bboxu
    def get_centered_x(self):
        return self. x0 + ((self.x1 - self.x0) / 2)

    # vrati y suradnicu stredu bboxu
    def get_centered_y(self):
        return self.y0 + ((self.y1 - self.y0) / 2)


    # vrati pomer stran vyska / sirka
    def get_box_ratio_height_to_width(self):
        w = 1.0 * self.get_box_width()
        h = 1.0 * self.get_box_height()
        return h / w

    #vrati vzdialenost stredov 2 bodov v pixeloch
    # teda vzdialenost seba a objektu v parametri
    #parametre: other = druhy objekt rovnakeho typu
    def center_distance_two_boxes(self, other):
        rect1x, rect1y = self.x0 + (self.x1 - self.x0) / 2, self.y0 + (self.y1 - self.y0) / 2
        rect2x, rect2y = other.x0 + (other.x1 - other.x0) / 2, other.y0 + (other.y1 - other.y0) / 2
        return sqrt((rect2x - rect1x) ** 2 + (rect2y - rect1y) ** 2)

    # vrati pocet pixelov prieniku tohto a objektu zadaneho v parametri
    # other_object = druhy objekt rovnakeho typu
    def intersection(self, other_object):
        rect1 = self.getBbox()
        rect2 = other_object.getBbox()
        if not isinstance(rect1, np.ndarray):
            rect1 = np.array(rect1)
        if not isinstance(rect2, np.ndarray):
            rect2 = np.array(rect2)
        rect1 = [min(rect1[[0, 2]]), min(rect1[[1, 3]]),
                 max(rect1[[0, 2]]), max(rect1[[1, 3]])]
        rect2 = [min(rect2[[0, 2]]), min(rect2[[1, 3]]),
                 max(rect2[[0, 2]]), max(rect2[[1, 3]])]
        return (max(0, min(rect1[2], rect2[2]) - max(rect1[0], rect2[0])) *
                max(0, min(rect1[3], rect2[3]) - max(rect1[1], rect2[1])))


    # vrati pocet pixelov zjednotenia tohto objektu a objektu zadaneho v parametri
    # other_object = objekt typu RectObject
    def union(self, other_object):
        rect1 = self.getBbox()
        rect2 = other_object.getBbox()
        intsctn = self.intersection(other_object)
        return (rect1[2] - rect1[0]) * (rect1[3] - rect1[1]) + (rect2[2] - rect2[0]) * (rect2[3] - rect2[1]) - intsctn


    # vrati pomer IoU tohto a objektu zadaneho v parametri
    # other_object = objekt typu RectObject
    def int_over_union(self, other_object):
        intersection = self.intersection(other_object)
        union = self.union(other_object)
        return intersection * 1.0 / max(union, .00001)


    #vrati pomer plochy prieniku tohto a druheho objektu, ku ploche tohto objektu
    # other_object = objekt typu RectObject
    def int_over_object_area(self, other_object):
        intersection = self.intersection(other_object)
        area = self.get_box_area()
        return intersection * 1.0 / max(area, .00001)


    # vrati True, ak tento a druhy objekt zadany v parametri maju nenulovy prienik
    # other_object = objekt typu RectObject
    def has_intersection(self, other_object):
        rect1 = self.getBbox()
        rect2 = other_object.getBbox()
        if not isinstance(rect1, np.ndarray):
            rect1 = np.array(rect1)
        if not isinstance(rect2, np.ndarray):
            rect2 = np.array(rect2)
        rect1 = [min(rect1[[0, 2]]), min(rect1[[1, 3]]),
                 max(rect1[[0, 2]]), max(rect1[[1, 3]])]
        rect2 = [min(rect2[[0, 2]]), min(rect2[[1, 3]]),
                 max(rect2[[0, 2]]), max(rect2[[1, 3]])]
        intersection = (max(0, min(rect1[2], rect2[2]) - max(rect1[0], rect2[0])) *
                        max(0, min(rect1[3], rect2[3]) - max(rect1[1], rect2[1])))
        return intersection > 0

