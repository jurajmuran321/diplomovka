from RectObject import *
from random import randint
import cv2
import config as cfg

# reprezentacia trasovanieho objektu
class TrackedObject(RectObject):

    next_obj_id = 0

    #konstruktor
    #parametre:
    # suradnice obejktu - suradnice laveho horneho a praveho dolneho rohu
    # trieda objektu
    # doveryhodnost prvotnej detekcie
    def __init__(self, x0, y0, x1, y1, clas, conf):
        RectObject.__init__(self, -1, x0, y0, x1, y1)
        self.clas = clas
        self.confidence = conf
        #self.new = True #prvykrat najdeny cez detekciu
        #farba tracku pri kresleni
        self.track_color_rgb = (randint(20, 240), randint(20, 240), randint(20, 240))

        # vsetky doterajsie body kde sa objekt nachadzal vo formate [ x,y centrovane na stred bboxu]
        self.locations = []
        self.locations.append([x0 + (x1 - x0) / 2, y0 + (y1 - y0) / 2])

        # vsetky doterajsie bboxy objektu s presnymi suradnicami [x0, y0, x1, y1]
        #self.history = []
        #self.history.append([x0, y0, x1, y1])

        self.last_score = 0
        self.candidates = []  #kandidati na objekt pri trasovani
        self.candidate_winner = None #vitazny kandidat

        self.times_not_found = 0

        self.kf = cv2.KalmanFilter(4, 2)
        #cent_x = x0 + (x1 - x0) / 2
        #cent_y = y0 + (y1 - y0) / 2
        #self.kf.measurementMatrix = np.array([[cent_y, 0, 0, 0], [0, cent_y, 0, 0]], np.float32)
        self.kf.measurementMatrix = np.array([[1, 0, 0, 0], [0, 1, 0, 0]], np.float32)
        self.kf.transitionMatrix = np.array([[1, 0, 1, 0], [0, 1, 0, 1], [0, 0, 1, 0], [0, 0, 0, 1]], np.float32)

    # prirad objektu track-id
    def assign_id(self):
        self.new = False
        self.id = TrackedObject.next_obj_id
        TrackedObject.next_obj_id += 1


    # aktualizuj poziciu objektu#a uloz ju do historie
    #parametre:
    # box = nova pozicia [x0, y0, x1, y1]
    def update_position(self, box):
        self.x0 = int(box[0])
        self.y0 = int(box[1])
        self.x1 = int(box[2])
        self.y1 = int(box[3])
        self.locations.append([self.x0 + (self.x1-self.x0)/2, self.y0 + (self.y1-self.y0)/2])
        #self.history.append([self.x0, self.y0, self.x1, self.y1])

    # nakresli objekt aj jeho track
    # parametre: obrazok vo formate BGR nacitany cez opencv
    def draw_track(self, img):
        # track ak su aspon 2 lokacie
        if self.locations > 1:
            for i in range(0, self.locations.__len__() - 1, 1):
                xy = self.locations[i]
                xy2 = self.locations[i+1]
                cv2.line(img, (xy[0], xy[1]), (xy2[0], xy2[1]), self.track_color_rgb, 3) # color (0, 255, 0)
        # bounding-box a id
        cv2.rectangle(img, (self.x0, self.y0), (self.x1, self.y1), [0, 0, 255], 2)
        x, y = self.x0 + (self.x1 - self.x0) / 2, self.y0 + (self.y1 - self.y0) / 2
        cv2.putText(img, str(self.id), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2, cv2.LINE_8)

    # nakresli len bbox objektu
    # parametre: obrazok vo formate BGR nacitany cez opencv
    def draw_box_only(self, img):
        cv2.rectangle(img, (self.x0, self.y0), (self.x1, self.y1), [0, 0, 255], 2)
        #ak chcem nakreslit aj id
        #x, y = self.x0 + (self.x1 - self.x0) / 2, self.y0 + (self.y1 - self.y0) / 2
        #cv2.putText(img, str(self.id), (x, y), cv2.FONT_HERSHEY_SIMPLEX, 0.75, (0, 255, 0), 2, cv2.LINE_8)


    #vrat suradnice objektu [left_top_xy, right_bottom_xy]
    def getBbox(self):
        return [self.x0, self.y0, self.x1, self.y1]

    # zisti ci vysiel objekt mimo zaber
    # return:
    # vrati True, ak sa niektora zo suradnic STREDU OBJEKTU dostane na hranicu obrazu
    def is_out_of_view(self):


        x0 = self.get_centered_x()
        y0 = self.get_centered_y()
        x1 = x0 + self.get_box_width()/2
        y1 = y0 + self.get_box_height()/2
        if x0 <=0 or x1 >= cfg.image_w or \
                y0 <= 0 or y1 >= cfg.image_h:
            return True
        return False


    #odhadni novu polohu objektu, ak si objekt pri trasovani nenasiel,
    # na zaklade jeho predoslych poloh odhadni novu polohu; ponechaj povodne rozmery boxu
    # return:
    # nova odhadnuta poloha [x0, y0, x1, y1]
    def estimate_next_location(self):
        #ponechaj povodne rozmery boxu
        w = self.get_box_width()
        h = self.get_box_height()
        #odhadni poziciu
        predicted = self.kf.predict()
        #ak bol najdeny menej ako 10-krat, KF predikcia nebude fungovat, odhadni novu polohu podla poslednych 2 lokacii
        if len(self.locations) - self.times_not_found < 10:
            if len(self.locations) < 2:
                new_x0 = self.x0
                new_y0 = self.y0
            else:
                new_x0 = self.x0 + self.locations[-1][0] - self.locations[-2][0]
                new_y0 = self.y0 + self.locations[-1][1] - self.locations[-2][1]
        #ak odhadujes, povazuj odhad za spravny
        else:
            new_x0 = predicted[0] - (w / 2)
            new_y0 = predicted[1] - (h / 2)
        last_pos = np.array([[np.float32(new_x0 + w / 2)], [np.float32(new_y0 + h / 2)]])
        # zadaj spravnu polohu
        self.kf.correct(last_pos)
        #new_x0 = predicted[0] - (w / 2)
        #new_y0 = predicted[1] - (h / 2)
        #odhad povazuj za spravny a zadaj ho tak aj filtru
        #new_pos = np.array([[np.float32(new_x0)], [np.float32(new_y0)]])
        #self.kf.correct(new_pos)
        return [new_x0, new_y0, new_x0 + w, new_y0 + h]