from RectObject import *
from hlp_math import do_intersect_2_lines

# trieda reprezentujuca oblast EXIT, na ktoru ked narazi objekt, konci sa jeho trasovanie
class MyExit(RectObject):

    next_exit_id = 0

    #konstruktor
    # id objektu, suradnice laveho horneho a praveho dolneho rohu
    def __init__(self, x0, y0, x1, y1):
        RectObject.__init__(self, MyExit.next_exit_id, x0, y0, x1, y1)
        MyExit.next_exit_id += 1

    #vrati True, ak dojde k prieniku tohto exitu (ciary) s objektom v parametri (obdlznikom)
    # other_object = objekt typu RectObject
    def has_intersection_line(self, other_object):
        bbox = other_object.getBbox()
        #zober ciaru exitu a postupne ber po jednej v smere hod. ruciciek strany obdlznika
        #a zistuj ci existuje prienik tychro dvoch stran, ak dojde k prieniku s ciarou, doslo aj prieniku s odblznikom
        ex0, ex1 = (self.x0, self.y0), (self.x1, self.y1)
        bx0, by0 = bbox[0], bbox[1]
        bx1, by1 = bbox[2], bbox[3]
        if do_intersect_2_lines(ex0, ex1, (bx0, by0), (bx1, by0)): #horna ciara
            return  True
        if do_intersect_2_lines(ex0, ex1, (bx1, by0), (bx1, by1)): #prava ciara
            return  True
        if do_intersect_2_lines(ex0, ex1, (bx1, by1), (bx0, by1)): #dolna ciara
            return  True
        if do_intersect_2_lines(ex0, ex1, (bx0, by1), (bx0, by0)): #lava ciara
            return  True
        return False

