

#objekt typu QuadTree, nejedna sa o vlastnu implementaciu
# zdroj
# http://www.pygame.org/wiki/QuadTree
class QuadTree(object):
    """An implementation of a quad-tree.

    This QuadTree started life as a version of [1] but found a life of its own
    when I realised it wasn't doing what I needed. It is intended for static
    geometry, ie, items such as the landscape that don't move.

    This implementation inserts items at the current level if they overlap all
    4 sub-quadrants, otherwise it inserts them recursively into the one or two
    sub-quadrants that they overlap.

    Items being stored in the tree must possess the following attributes:

        x0 - the x coordinate of the x0 edge of the item's bounding box.
        y0 - the y coordinate of the y0 edge of the item's bounding box.
        x1 - the x coordinate of the x1 edge of the item's bounding box.
        y1 - the y coordinate of the y1 edge of the item's bounding box.

        where x0 < x1 and y0 < y1

    ...and they must be hashable.

    Acknowledgements:
    [1] http://mu.arete.cc/pcr/syntax/quadtree/1/quadtree.py
    """

    def __init__(self, items, depth=8, bounding_rect=None):
        """Creates a quad-tree.

        @param items:
            A sequence of items to store in the quad-tree. Note that these
            items must possess x0, y0, x1 and y1 attributes.

        @param depth:
            The maximum recursion depth.

        @param bounding_rect:
            The bounding rectangle of all of the items in the quad-tree. For
            internal use only.
        """
        # The sub-quadrants are empty to start with.
        self.nw = self.ne = self.se = self.sw = None

        # If we've reached the maximum depth then insert all items into this
        # quadrant.
        depth -= 1
        if depth == 0:
            self.items = items
            return

        # Find this quadrant's centre.
        if bounding_rect:
            l, t, r, b = bounding_rect
        else:
            # If there isn't a bounding rect, then calculate it from the items.
            l = min(item.x0 for item in items)
            t = min(item.y0 for item in items)
            r = max(item.x1 for item in items)
            b = max(item.y1 for item in items)
        cx = self.cx = (l + r) * 0.5
        cy = self.cy = (t + b) * 0.5

        self.items = []
        nw_items = []
        ne_items = []
        se_items = []
        sw_items = []

        for item in items:
            # Which of the sub-quadrants does the item overlap?
            in_nw = item.x0 <= cx and item.y0 <= cy
            in_sw = item.x0 <= cx and item.y1 >= cy
            in_ne = item.x1 >= cx and item.y0 <= cy
            in_se = item.x1 >= cx and item.y1 >= cy

            # If it overlaps all 4 quadrants then insert it at the current
            # depth, otherwise append it to a list to be inserted under every
            # quadrant that it overlaps.
            if in_nw and in_ne and in_se and in_sw:
                self.items.append(item)
            else:
                if in_nw: nw_items.append(item)
                if in_ne: ne_items.append(item)
                if in_se: se_items.append(item)
                if in_sw: sw_items.append(item)

        # Create the sub-quadrants, recursively.
        if nw_items:
            self.nw = QuadTree(nw_items, depth, (l, t, cx, cy))
        if ne_items:
            self.ne = QuadTree(ne_items, depth, (cx, t, r, cy))
        if se_items:
            self.se = QuadTree(se_items, depth, (cx, cy, r, b))
        if sw_items:
            self.sw = QuadTree(sw_items, depth, (l, cy, cx, b))

    def hit(self, rect):
        """Returns the items that overlap a bounding rectangle.

        Returns the set of all items in the quad-tree that overlap with a
        bounding rectangle.

        @param rect:
            The bounding rectangle being tested against the quad-tree. This
            must possess x0, y0, x1 and y1 attributes.
        """

        def overlaps(item):
            return rect.x1 >= item.x0 and rect.x0 <= item.x1 and \
                                                                rect.y1 >= item.y0 and rect.y0 <= item.y1

        # Find the hits at the current level.
        hits = set(item for item in self.items if overlaps(item))

        # Recursively check the lower quadrants.
        if self.nw and rect.x0 <= self.cx and rect.y0 <= self.cy:
            hits |= self.nw.hit(rect)
        if self.sw and rect.x0 <= self.cx and rect.y1 >= self.cy:
            hits |= self.sw.hit(rect)
        if self.ne and rect.x1 >= self.cx and rect.y0 <= self.cy:
            hits |= self.ne.hit(rect)
        if self.se and rect.x1 >= self.cx and rect.y1 >= self.cy:
            hits |= self.se.hit(rect)

        return hits

