from RectObject import *

# objekt reprezentujuci oblast ROI, kde dochadza k hladaniu novych objektov pre trasovanie
class MyRoi(RectObject):

    next_roi_id = 0

    #konstruktor
    # id objektu, suradnice laveho horneho a praveho dolneho rohu
    def __init__(self, x0, y0, x1, y1):
        RectObject.__init__(self, MyRoi.next_roi_id, x0, y0, x1, y1)
        MyRoi.next_roi_id += 1