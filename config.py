
'''subor obsahuje vsetky konstanty a "premenne" konstanty, ktore mozno menit'''

'''premenne'''



#cesta k suboru s obrazkami videa
datasetPath = '/home/juraj/dataset/hliny2'


#typ trasovania
# 1 = trasovanie iba pomocou trackera Re3, inicializacia detekciou yolo 2
# 2 = trasovanie iba cez spajanie detekcii z detektora yolo 2
# 3 = trasovanie iba cez spajanie detekcii z detektora yolo 3
# 4 = trasovanie kombinacie trackera a spajania detekcii z yolo2
tracker_type = 2
# ak je True, vykreslia sa pri detekcii uplne vsetky detekcie
draw_all_detections = False

#plati len pre spajanie detekcii na debugovanie; vykresli kandidatov a ich skore
debug_candidates = False

#ak True, vystupy trasovania sa ulozia ako obrazky s vykreslenymi objektami
save_output_image = False

#ak True, vysledky trasovania sa ulozia do xml dokumentu
save_output_xml = False


#parametre snimky - nastavi sa automaticky
im_format = 'png'
image_w = 0
image_h = 0

#nastavenie darknet
darknet_version = 3
detection_period_roi = 10 #ako casto sa robi detekcia novych objektov v roi
det_classes = {'car', 'van', 'bus', 'truck' } # triedy z detekcie ktore sa akceptuju
det_conf_tresh = .5 #ber len detekcie s confidence vyssou/rovnou ako je tato hodnota

'''konstatnty'''
#IoA dvoch objektov typu RectObject = pomer prieniku tychto objetov / plocha prveho z objektov
# ak detekovany objekt ma IoA 0.8 s roi, beriem ho ze padol do roi
min_roi_IoA = 0.8
# ak ma novy detekovany objekt aspon takyto IoA s trasovanym, beriem ze je to ten isty; == max akceptovane prekrytie
max_tracked_detected_IoA = 0.5 #predtymm 0.2
# ak objekt zasiahne do ignore menej ako tato hodnota IoA, este sa nezahodi
max_ignore_IoA = 0.2

# minimalna a maximalna velkost detekovaneho objektu ktoru akceptujeme
min_object_h = 25
min_object_w = 25
max_object_h = 500
max_object_w = 500

#maximalna hodnota pomeru max. strana / min. strana, napr. 3 znamena pomer 3:1
max_object_ratio = 3
   #ak ma detekovany bbox aspon taketo IoU s lubovolnym trasovanym objektom, ponecha sa, inak sa zrusi
   #min_detection_tracked_IoU = 0.6
#ak maju 2 detekcie IoU tolkoto alebo viac, berie sa ze je to ten isty objekt
max_detections_IoU = 0.9

#vahy jednotlivych porovnani pre trasovanie
w_IoU = 3
w_shift = 3
w_height = 1
w_width = 1
w_ratio = 1

#suma vah
w_sum = w_IoU + w_shift + w_height + w_width + w_ratio

#ak ma vitazny kandidat o tolkoto vacsie skore ako posledne zaznamenane skore, odmietneme ho
max_delta_score = w_sum*15

#ak sa objekt nenajde tolkotokrat za sebou, rusi sa ako strateny
max_not_found_delete = 15

