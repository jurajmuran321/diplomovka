import glob
import sys
import os.path
import copy
import time

from Objects.TrackedObject import *
import My_trackers.my_tracker_re3 as mt

#toto znamena ze nebudem pouzivat detektor a netreba ho nacitavat do gpu
cfg.darknet_version = -1
import helper as h


paused = True
image = None
image_backup = None

tracked_objects = []
new_objects = []

drawing = False # true if mouse is pressed
ix,iy = 0,0
draw_type = "roi"


def draw_rect(event,x,y,flags,param):
    global ix,iy,drawing,paused,draw_type, image, image_backup

    if not paused:
        return #kresli len ked je pauznute video
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            image = copy.copy(image_backup)
            cv2.rectangle(image, (ix, iy), (x, y), (255, 0, 0), 2)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        new_obj = TrackedObject(ix, iy, x, y, 'traffic', 1)
        new_obj.assign_id()
        new_objects.append(new_obj)


video = "video"
cv2.namedWindow(video, cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback(video, draw_rect)



basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(basedir, os.path.pardir)))
image_paths = sorted(glob.glob(os.path.join(cfg.datasetPath, '*.' + cfg.im_format)))


def main(args):
    global paused, new_objects, tracked_objects

    h.parse_and_set_args(args)
    firstRun = True
    frame_num = 1
    image_save_dir=""
    xml_data_string=""
    timeSum = 0.0

    if cfg.save_output_image:
        image_save_dir = cfg.datasetPath + '_out/'
        if not os.path.exists(image_save_dir):
            os.makedirs(image_save_dir)

    if cfg.save_output_xml:
        xml_data_string = ""

    for image_path in image_paths:
        if firstRun:
            image, image_backup = h.load_image(image_path)
            assert image is not None, 'chyba, nepodarilo sa nacitat obrazok'
            cfg.image_w, cfg.image_h = h.get_image_size(image_path)
            cv2.imshow(video, image)
            cv2.waitKey(1)
            firstRun = False
            #doDetection = True
        else:
            image, image_backup = h.load_image(image_path)
        #daj cas na nakreslenie bboxu
        while(paused):
            h.draw_everything(image, tracked_objs=new_objects)
            cv2.imshow(video, image)
            key = cv2.waitKey(100)
            #h.draw_everything(image, tracked_objs=tracked_objects)
            if key == ord('p'):
                paused = False

        imageRGB = image[:, :, ::-1]  # Tracker expects RGB, but opencv loads BGR.

        start = time.time()  # time
        #trasovanie
        tracked_objects = mt.track_image(imageRGB, tracked_objects, new_objects)
        #ak si v tomto kroku robil inicializaciu novymi boxami, vynuluj ich
        if len(new_objects) > 0:
            new_objects = []
        #skontroluj podmienky pre zrusenie objektu a vyrad ak treba
        tracked_objects = h.check_tracked_for_exit(tracked_objects)
        timeSum += time.time() - start  # time

        h.draw_everything(image, tracked_objs=tracked_objects, draw_tracks=False)
        cv2.imshow(video, image)

        if cfg.save_output_image:
            path = image_save_dir + format(frame_num, '06') + "." + cfg.im_format
            cv2.imwrite(path, image)
        if cfg.save_output_xml:
            xml_data_string += h.convert_tracked_to_xml(tracked_objects, image_path)

        frame_num += 1
        key = cv2.waitKey(1)
        if key == ord('p'): #pauzni
            tracked_objects = []
            new_objects = []
            paused = True
        elif key == ord('r'): #resetuj aktualne trackypp
            tracked_objects = []
        elif key == 27: #esc -ukonci beh
            break;

    if cfg.save_output_xml:
        h.saveXml(xml_data_string)

    print 'Finished tracking.'
    print 'Average time per frame (secs) = ' + str(round(timeSum / frame_num, 3))  # time

    return 0


if __name__ == "__main__":
    main(sys.argv)