### Zdrojové kódy ku diplomovej práci "Trasovanie objektov" 

Tento dokument obsahuje pokyny na inštaláciu a spustenie aplikácie.

#### 1. Inštalácia:
Program je určený na inštaláciu a beh na operačnom systéme Linux, konkrétne bol vyvíjaný a testovaný na Ubuntu 16.04. Pre beh GPU implementácií je potrebná grafická karta Nvidia. So softvérového hľadiska sú pre inštaláciu sú nasledovné požiadavky:

- Python 2.7
- Tensorflow (tensorflow-gpu 1.5.0)
- NumPy
- OpenCV 2
- CUDA (odporúčané)
- cuDNN (odporúčané)

Návod a pomoc ako dané knižnice nainštalovať je možné nájsť priamo na stránkach použitých projektov Re3 a Darknet YOLO:

1. Re3: https://gitlab.com/danielgordon10/re3-tensorflow
2. YOLO 2: https://pjreddie.com/darknet/yolov2/
3. YOLO 3: https://pjreddie.com/darknet/install/

Pre rozbehnutie detektora je potrebné naklonovať si na disk projekt z repozitára [YOLO 2](https://pjreddie.com/darknet/yolov2/) a [YOLO 3](https://pjreddie.com/darknet/install/) a nainštalovať podľa inštrukcií. Pre naklonovanie YOLO3 je potrebné v repozitári zmeniť Branch na **yolo3**, 
defaultná master Branch je YOLO2. Následne je treba pre obe verzie v priečinku YOLO2 projektu nájsť súbor **libdarknet.so**, rovnako tak v prípade YOLO3 kde ho navyše treba premenovať na **libdarknet3.so**, a oba tieto súbory nakopírovať do priečinku projektu Diplomovka, presne na umiestnenie _/Detekcia/libs_. 
Pre inštaláciu trackera Re3 je potrebné sa presunúť do priečinka _/Re3_tracker_. Následne treba tracker naištalovať podľa návodu v priloženom súbore **README.md** v tomto podpriečinku.
Pre následnú prácu s projektom, resp. prípadné riešenie problémov s knižnicami je odporúčané vývojové prostredie [PyCharm](https://www.jetbrains.com/pycharm/).

[] () {}

#### 2. Používateľská príručka

Po úspešnej inštalácií je možné program spustiť. Spúšťacím súborom pre trasovanie áut je to **tracking.py**. Spustenie buď cez PyCharm alebo cez konzolu príkazom:
    
    python tracking.py [prepínače]
    
Parametre behu je možné nastaviť cez prepínače, alebo priamo v súbore **config.py**. 
Vysvetlenie jednotlivých premenných a konštánt je podrobne popísané v uvedenom súbore. Najdôležitejšie pre spustenie sú nasledovné:

- _datasetPath_ – cesta k testovanému videu; je nutné, aby bolo video rozparsované na zoznam obrázkov, na čo je možné použiť priložený skript **parse_video.sh**
    -  -input CESTA_K_PRIECINKU_VIDEA
- _tracker_type_ – identifikačné číslo trasovacieho algoritmu:
    - 1 = trasovanie iba pomocou trackera Re3, inicializácia detekciou yolo 2
    - 2 = trasovanie iba cez spájanie detekcii z detektora yolo 2
    - 3 = trasovanie iba cez spájanie detekcii z detektora yolo 3
    - 4 = trasovanie kombinacie trackera a spajania detekcii z yolo 2
        - -algo ID_CISLO_ALGORITMU
- _save_output_image_ = True / False - ak True, výstupy trasovania sa uložia na disk ako obrázky s vykreslenými objektami
    - -simg
- _save_output_xml_ = True / False - ak True, výsledky trasovania sa uložia na disk do xml dokumentu
    - -sxml
 - _draw_all_detections_ = True / False - ak je True, vykreslia sa pri detekcii úplne všetky nájdené detekcie
    - -ddet
- _debug_candidates_ = True / False – vykresli postupne pre každý trasovaný objekt: box trasovaného objektu a postupne všetkých jeho kandidátov spolu s ich skóre (platí len pre spájanie detekcii, na debugovanie)
    - -dbg

Posledné 4 premenné s hodnotami True / False sú defaultne False.


Po spustení sa zobrazí prvá snímka videa. Tu môže užívateľ nakresliť oblasti **ROI**, **EXIT** a **IGNORE**. Počas kreslenia sa dá prepínať stlačením numerických kláves „1“ = kreslenie ROI,
 „2“ = kreslenie EXIT „3“ = kreslenie IGNORE; prípadne aktuálne nakreslené oblasti vynulovať a začať kreslenie odznova klávesou „0“ Je možné nakresliť ľubovoľný počet každej z oblastí.
 
1. ROI – kreslenie oblastí (pravouholník), kde sa detekujú nové objekty na trasovanie. Sú vykreslené zelenou farbou, a ak užívateľ žiadne ROI nezadá, berie sa ako roi celý záber v snímke.
2. EXIT – kreslenie oblastí (čiara), ktoré rušia trasované objekty pri vstupe do nich. Sú vykreslené modrou farbou a nie je povinné ich zadávať
3. IGNORE – kreslenie oblastí (pravouholník), kde neprebieha detekcia ani trasovanie. Sú vykreslené čiernou farbou a nie je povinné ich zadávať

Po nakreslení oblastí môžeme video spustiť klávesou „p“ a začne sa trasovanie. Stlačení klávesy „p“ (= pause) možno potom trasovanie kedykoľvek pozastaviť a opäť spustiť. Počas pauzy sa dá opäť prepnúť do módu kreslenia stlačením klávesy „0“ a nakresliť nové oblasti ROI, EXIT a IGNORE.
V prípade zapnutého módu debugovania (debug_candidates = True) je možné sledovať výpočet skóre kandidátov. Postupne ako prebieha trasovanie, pre každý trasovaný objekt sa postupne vykresľuje posledná známa poloha aktuálneho objektu z predošlého snímku a takisto všetci jeho kandidáti na novú polohu z aktuálneho snímku spolu s ich skóre vzhľadom na trasovaný objekt. Jednotlivé objekty a kandidáti sa vykresľujú postupne a video sa na každom z nich zastaví, pre postup trasovania sa treba posúvať stláčaním klávesy „c“ (=continue). Istý spôsob debugovania umožňuje aj možnosť draw_all_detections = True, ktorá pri trasovaní vždy po zbehnutí každej detekcie vykreslí na krátky čas všetky bounding-boxy objektov, 
ktoré sa našli počas detekcie. V tomto samotnom prípade sa už video nezastavuje, ale je možné použiť obe tieto možnosti zároveň, kde už k zastavovaniu dochádza a treba ho opäť posúvať použitím klávesy „p“. Počas trasovania je ešte možno použiť klávesu „r“ (=reset), ktorá zruší všetky aktuálne trasované objekty a trasuje od tohto momentu opäť znova „od nuly“, s tým že zaznačené oblasti typu ROI zostanú bez zmeny.

Čo sa týka ukladania výstupov, obrázky sa ukladajú v rovnakej veľkosti a formáte ako pôvodné obrázky v priečinku kde je zdrojové video a s rovnakým názvom ako je názov videá s príponou _out. Obrázky je možné spojiť do videa použitím skriptu join_images_to_video.sh. Výstupný xml súbor sa ukladá v rovnakom  riečinku ako je priečinok videa, pod rovnakým názvom ako má video s príponou .xml. Xml je vo formáte xml2, ktorý je možné otvoriť v Anotačnom nástroji vyvíjanom na FRI počas projektovej výučby. Jeho štruktúra a vysvetlenie je popísaná aj v priečinku testovacieho datasetu ktorý je v prílohe práce.
