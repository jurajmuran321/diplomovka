import glob
import sys
import os.path
import time
#import copy
#import config as cfg


from Objects.MyRoi import *
from Objects.MyExit import *

from My_trackers.MyTracker import *
import helper as h

#PREMENNE
my_rois = []
my_exits = []
my_ignores = []
tracker = None

image = None
image_backup = None

paused = True
drawing = False # true ked je kliknuta mys
ix,iy = 0,0
draw_type = "roi"

basedir = os.path.dirname(__file__)
sys.path.append(os.path.abspath(os.path.join(basedir, os.path.pardir)))
image_paths = sorted(glob.glob(os.path.join(cfg.datasetPath, '*.' + cfg.im_format)))


def draw_rect(event,x,y,flags,param):
    global ix,iy,drawing,paused,draw_type, my_rois, my_exits, image, image_backup

    if not paused:
        return #kresli len ked je pauznute video
    if event == cv2.EVENT_LBUTTONDOWN:
        drawing = True
        ix,iy = x,y
    elif event == cv2.EVENT_MOUSEMOVE:
        if drawing == True:
            #image = copy.copy(image_backup)
            if draw_type == "roi":
                cv2.rectangle(image, (ix, iy), (x, y), (255, 0, 0), 2)
            elif draw_type == "exits":
                cv2.line(image, (ix, iy), (x, y), (255, 0, 0), 2)
            elif draw_type == "ignores":
                cv2.rectangle(image, (ix, iy), (x, y), (255, 0, 0), 2)
    elif event == cv2.EVENT_LBUTTONUP:
        drawing = False
        if draw_type == "roi":
            my_rois.append(MyRoi(int(ix), int(iy), int(x), int(y)))
        elif draw_type == "exits":
            my_exits.append(MyExit(int(ix), int(iy), int(x), int(y)))
        elif draw_type == "ignores":
            my_ignores.append(MyRoi(int(ix), int(iy), int(x), int(y))) # ignore area je tiez objekt typu MyRoi


video = "video"
cv2.namedWindow(video, cv2.WINDOW_AUTOSIZE)
cv2.setMouseCallback(video, draw_rect)
timeSum = 0.0


def main(args):
    global my_rois, my_exits, my_ignores, \
    paused, draw_type, timeSum, image

    h.parse_and_set_args(args)

    tracked_objects = []
    firstRun = True
    frame_num = 1
    image_save_dir=""
    xml_data_string=""
    tracker=MyTracker(cfg.tracker_type, my_rois, my_exits, my_ignores)

    if cfg.save_output_image:
        image_save_dir = cfg.datasetPath + '_out/'
        if not os.path.exists(image_save_dir):
            os.makedirs(image_save_dir)

    if cfg.save_output_xml:
        xml_data_string = ""

    for image_path in image_paths:

        #nacitaj obrazok
        if firstRun:
            image, image_backup = h.load_image(image_path)
            assert image is not None, 'chyba, nepodarilo sa nacitat obrazok'
            #inicializacia
            cfg.image_w, cfg.image_h = h.get_image_size(image_path)
            cfg.im_format = h.get_image_format(image_path)
            cv2.imshow(video, image)
            cv2.waitKey(1)
            firstRun = False
            tracker = MyTracker(cfg.tracker_type, my_rois, my_exits, my_ignores)
        else:
            image, image_backup = h.load_image(image_path)

        #ak je pauza, daj cas na nakreslenie roi, exit, ignore
        while (paused):
            image = copy.copy(image_backup)
            h.draw_everything(image, my_rois, my_exits, my_ignores, tracked_objects)
            cv2.imshow(video, image)
            key = cv2.waitKey(100)
            if key == ord('0'):
                # vsetko vynuluj a kresli odznova
                my_rois = []
                my_exits = []
                my_ignores = []
                tracked_objects = []
                h.draw_everything(image, my_rois, my_exits, tracked_objects)
            if key == ord('1'):
                draw_type = "roi"
            elif key == ord('2'):
                draw_type = "exits"
            elif key == ord('3'):
                draw_type = "ignores"
            elif key == ord('p'):
                # ak nebolo zadane ziadne roi, tak daj cely obrazok ako roi
                if len(my_rois) == 0:
                    my_rois.append(MyRoi(0, 0, cfg.image_w, cfg.image_h))
                tracker.set_areas(my_rois, my_exits, my_ignores)
                paused = False

        #trasuj, vykresli a uloz
        start = time.time() #time
        tracked_objects = tracker.track(tracked_objects, image, image_path, frame_num)
        timeSum += time.time() - start #time

        h.draw_everything(image, my_rois, my_exits, my_ignores, tracked_objects)
        cv2.imshow(video, image)

        if cfg.save_output_image:
            path = image_save_dir + format(frame_num, '06') + "." + cfg.im_format
            cv2.imwrite(path, image)
        if cfg.save_output_xml:
            xml_data_string += h.convert_tracked_to_xml(tracked_objects, image_path)

        frame_num += 1
        key = cv2.waitKey(1)
        if key == ord('0'):  # resetuj roi
            paused = True
            draw_type = "roi"
            #my_rois = []
            #my_exits = []
            #my_ignores = []
        elif key == ord('p'):  # pauzni
            paused = True
        elif key == ord('r'):  # resetuj tracky
            tracked_objects = []
        elif key == 27:  # esc -ukonci beh
            break;

    if cfg.save_output_xml:
        h.saveXml(xml_data_string)

    print 'Finished tracking.'
    print 'Average time per frame (secs) = ' + str(round(timeSum / frame_num, 3))  # time

    return 0





if __name__ == "__main__":
    main(sys.argv)
